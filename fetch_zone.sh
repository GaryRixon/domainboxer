#!/usr/bin/env bash

set -e
set -u

# Catch exit codes and print an error message
function cleanup {
    if [ $? != 0 ]; then
        echo "ERROR: There was a problem when exporting the zone for ${DOMAIN}. Exiting"
        exit 1
    else
        echo "Done!"
    fi
}
trap cleanup EXIT

# I'm sure there are better ways to do this, but it works...
cli53 export $1 | sed '1,2d' | grep -v "NS\|ALIAS" | cut -f1,4,5 | tr '\t' ' ' > ${1}.ready

exit 0
