#!/usr/bin/env python2.7 -c

import requests
import json
import jmespath
import sys
import argparse
import subprocess

# The DomainBox API endpoint
endpoint = 'https://live.domainbox.net/api/JsonAPI.asmx/'
# Headers to send with POST requests
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
# Authorisation params we need to give when executing requests against the API
auth_params = {"AuthenticationParameters": {"Reseller": "dogsbody","Username": "dogsbodytech","Password": "ZqJBb2Se3sYcKCbS9KqQ*"}}

def main(argv):
	# Create our argument parser
    parser = argparse.ArgumentParser(description='This is my argument parser program!')
    # Specify that we accept an argument (-d) which is to be our domain name
    parser.add_argument('-d', action="store", dest="domain")

    # Retreive the domain we'll be working with from the argument
    domain = parser.parse_args().domain

    modifyNameservers(domain)


# This function simply POSTs combines the auth paramaters and the actual command paramaters, issues them against the API, and returns the results
def execute(command, command_params):
    url = endpoint + command
    full_params = auth_params
    full_params.update(command_params)
    return requests.post(url, json.dumps(full_params), headers=headers)

# Check that a command was executed successfully based by on the response from the API
def checkSuccess(response):
    # Extact the result code from the response and check it
    result_code = jmespath.search('d.ResultCode', response)
    # Check the code starts with a '1', this indicates success
    if (str(result_code))[:1] == '1':
        return True
    else:
        return False

def extractResultMessage(response):
    return jmespath.search('d.ResultMsg', response)

# Switch the NS records for a given domain to use DomainBox's name servers
def modifyNameservers(domain):
    print 'Switching NS records for', domain + '...'
    # The JSON string to set name servers to Domain Box's default name servers
    command_params = {'CommandParameters': {'Nameservers': {'NS11': '', 'NS8': '', 'NS9': '', 'NS10': '', 'NS13': '', 'NS12': '', 'NS1': 'ns1.dnsfarm.org', 'NS2': 'ns2.dnsfarm.org', 'NS3': 'ns3.dnsfarm.org', 'NS4': '', 'NS5': '', 'NS6': '', 'NS7': ''}, 'AutoLockMode': '', 'DomainName': domain}}
    response = json.loads(execute('ModifyDomainNameservers', command_params).text)
    # Check our command was executed successfully and if not, output the error message
    if checkSuccess(response):
        print 'Name servers changed successfully:', domain
    else:
        print 'Error:', extractResultMessage(response)

if __name__ == "__main__":
    main(sys.argv[1:])
