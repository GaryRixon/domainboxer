This project aims to make transferring your DNS from Route53 to DomainBox as easy as possible.

# Installation / Setup

## cli53 (required)
```
wget https://github.com/barnybug/cli53/releases/download/0.7.4/cli53-linux-amd64
sudo mv cli53-my-platform /usr/local/bin/cli53
sudo chmod +x /usr/local/bin/cli53
```
You'll then need to configure your AWS crednetials by placing them in ```~/.aws/credentials```

```
[default]
aws_access_key_id = <access_key_here>
aws_secret_access_key = <secret_key_here>
```

## PIP
```
sudo apt-get update
sudo apt-get install python-pip
```

## Clone the repo
```
git clone git@bitbucket.org:GaryRixon/r53-dbox.git /some/place/on/your/system
cd /some/place/on/your/system
```

## Install python dependencies
```
pip install -r requirements.txt
```

## Create your DomainBox credentials file

```
[domainbox]
reseller =
username =
password =
```
## Run It!

That's it, you can now use the code to export a zone from Route53 and add it DomainBox, like so:

```
./create_zone_add_records.py -d example.com

```

There is also a script to switch the name servers for a given domain to use DomainBox's name servers, which is ran in much the same way:

```
./switch-nameservers.py -d example.com
```