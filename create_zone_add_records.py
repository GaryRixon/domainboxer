#!/usr/bin/env python2.7 -c

import requests
import json
import jmespath
import sys
import argparse
import subprocess
import ConfigParser

# Open a our config file for reading
config = ConfigParser.RawConfigParser()
config.read('domainbox.credentials')
# Read in our DomainBox credentials
dbox_reseller = config.get('domainbox', 'reseller')
dbox_username = config.get('domainbox', 'username')
dbox_password = config.get('domainbox', 'password')

# The DomainBox API endpoint
endpoint = 'https://live.domainbox.net/api/JsonAPI.asmx/'
# Headers to send with POST requests
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
# Authorisation params we need to give when executing requests against the API
auth_params = {"AuthenticationParameters": {"Reseller": dbox_reseller,"Username": dbox_username,"Password": dbox_password}}
def main(argv):
    
    # Create our argument parser
    parser = argparse.ArgumentParser(description='This is my argument parser')
    # Accept an argument (-d, the domain name) and that the argument's value is to be stored in the "domain" variable
    parser.add_argument('-d', action="store", dest="domain")

    # Retreive the domain we'll be working with from the argument
    domain = parser.parse_args().domain

    # Create a zone for the provided domain in domain box
    createDnsZone(domain)
    # Call our "fetch_zone" script which retrieves and processes the current zone file from route53
    print 'Fetching and processing zone file...'
    subprocess.call(['./fetch_zone.sh %s' %(str(domain))], shell=True)

    # Read our processed zone file line-by-line and populate an array with each of the records
    zone_file_to_open = domain + '.ready'
    # Open file for reading
    with open(zone_file_to_open) as zone:
        records_array = []
        # Split the file line-by-line
        for line in zone:
            # process the line and save it in the format we want
            records_array.append(line.strip('\n').split(' '))

    # For each of the records, create a corresponding record in domain box
    for record in records_array:
        # If our record is a "standard" record (A, TXT...)
        if len(record) == 3:
            createDnsRecord(domain, record[1], record[0], record[2])
        # If our record is an MX record, also pass the priority through
        else:
            createDnsRecord(domain, record[1], record[0], record[3], record[2])

# This function simply POSTs combines the auth paramaters and the actual command paramaters, issues them against the API, and returns the results
def execute(command, command_params):
    url = endpoint + command
    full_params = auth_params
    full_params.update(command_params)
    return requests.post(url, json.dumps(full_params), headers=headers)

# Check that a command was executed successfully based by on the response from the API
def checkSuccess(response):
    # Extact the result code from the response and check it
    result_code = jmespath.search('d.ResultCode', response)
    # Check the code starts with a '1', this indicates success
    if (str(result_code))[:1] == '1':
        return True
    else:
        return False

def extractResultMessage(response):
    return jmespath.search('d.ResultMsg', response)

# Create a new zone
def createDnsZone(domain):
    print 'Creating zone for', domain + '...'
    # Generate our command params
    command_params = {'CommandParameters':{'Zone': domain}}
    # Execute the command and store the response
    response = json.loads(execute('CreateDnsZone', command_params).text)
    # Check our command was executed successfully and if not, output the error message
    if checkSuccess(response):
        print 'Zone created successfully:', domain
    else:
        print 'Warning:', extractResultMessage(response)

# Add a record to a given zone
def createDnsRecord(domain, record_type, hostname, content, priority=10):
    print 'Creating record for', domain + '...'
    # Generate our command params
    if record_type == "MX":
        command_params = {'CommandParameters': {'Records': [{'RecordType': record_type, 'HostName': hostname, 'Content': content, 'Priority': priority}], 'Zone': domain}}
    elif record_type == "TXT":
        content = content[1:-1]
        command_params = {'CommandParameters': {'Records': [{'RecordType': record_type, 'HostName': hostname, 'Content': content}], 'Zone': domain}}    
    else:
        command_params = {'CommandParameters': {'Records': [{'RecordType': record_type, 'HostName': hostname, 'Content': content}], 'Zone': domain}}    
    # Execute the command and store the response
    response = json.loads(execute('CreateDnsRecords', command_params).text)
    # Check our command was executed successfully and if not, output the error message
    if checkSuccess(response):
        print 'Record created successfully:', record_type, hostname, domain, content
    else:
        print 'Warning:', extractResultMessage(response)

if __name__ == "__main__":
    main(sys.argv[1:])
